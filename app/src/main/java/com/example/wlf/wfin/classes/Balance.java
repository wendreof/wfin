package com.example.wlf.wfin.classes;

import java.io.Serializable;

public class Balance implements Serializable {

    private Integer id;

    private Customer customer;
    private double bill;
    private double income;
    private double saldo;
    private String incomeType;
    private String billType;

    public Balance(Customer customer) {
        this.customer = customer;
    }

    public Balance() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBill() {
        return bill;
    }

    public void setBill(double bill) {
        this.bill = bill;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getSaldo() {

        saldo = (income - bill);

        return Math.abs(saldo);
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
