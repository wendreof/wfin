package com.example.wlf.wfin.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.wlf.wfin.R;
import com.example.wlf.wfin.classes.Balance;
import com.example.wlf.wfin.classes.Customer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText nameCust, ageCust, emailCust;
    private Button btnNext;
    private Balance balance;
    private Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        nameCust = findViewById( R.id.nameCust );
        ageCust = findViewById( R.id.ageCust );
        emailCust = findViewById( R.id.emailCust );
        btnNext = findViewById( R.id.btnNext );

        balance = new Balance();

        btnNext.setOnClickListener( this );
    }

    @Override
    public void onClick( View view )
    {
        int a = view.getId();
        if ( a == R.id.btnNext )
        {
            newCustomer();
            balance.setCustomer( customer );
            exibirToast("Cliente cadastrado" );
            exibirDados();
        }
    }

    public void newCustomer()
    {
        customer = new Customer();
        customer.setFulName( nameCust.getText().toString() );
        customer.setAge( Integer.parseInt(ageCust.getText().toString()) );
        customer.setEmail( emailCust.getText().toString() );

    }
    public void exibirDados()
    {
        Intent i = new Intent( this, BalanceActivity.class );
        i.putExtra( "balance", balance );
        startActivity( i );
    }

    private void exibirToast( String msg )
    {
        Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
    }
}
