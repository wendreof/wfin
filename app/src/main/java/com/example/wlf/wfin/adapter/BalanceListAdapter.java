package com.example.wlf.wfin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.wlf.wfin.R;
import com.example.wlf.wfin.classes.Balance;

import java.util.List;

public class BalanceListAdapter extends BaseAdapter{

    private Context context;
    private List<Balance> list;

    public BalanceListAdapter(Context context, List<Balance> list) {
           this.context = context;
           this.list = list;
    }

    public int getCount() {
         return list.size();
    }

    public Object getItem(int i) {
            return list.get(i);
        }

    public long getItemId(int i) {
            return i;
        }

    public View getView(int i, View convertView, ViewGroup viewGroup) {
            Balance b = list.get(i);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.balances, null);

            TextView id = view.findViewById(R.id.txtId);
            id.setText("# "+String.valueOf(b.getId()));

            TextView income =  view.findViewById(R.id.txtIncome);
            income.setText("Renda R$: "+String.valueOf(b.getIncome()));

            TextView bill = view.findViewById(R.id.txtBill);
            bill.setText("Despesa R$: "+String.valueOf(b.getBill()));

            return view;
        }
    }




