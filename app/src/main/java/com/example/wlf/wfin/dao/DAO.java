package com.example.wlf.wfin.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DAO <T extends Object> extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "balance.sqlite3";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_BALANCE = "balance";

    protected Context context;
    protected String[] campos;
    protected String tableName;

    private static final String CREATE_TABLE_BALANCE = "CREATE TABLE balance ( "
            + " id integer primary key autoincrement NOT NULL,"
            + " salario VARCHAR( 35 ),"
            + " conta VARCHAR( 35 ));";

    public DAO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(CREATE_TABLE_BALANCE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion1) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_BALANCE);

        onCreate(db);
    }

    protected void closeDatabase(SQLiteDatabase db)
    {
        if(db.isOpen())
            db.close();
    }
}
