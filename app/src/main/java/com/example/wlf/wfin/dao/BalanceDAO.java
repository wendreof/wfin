package com.example.wlf.wfin.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.wlf.wfin.classes.Balance;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class BalanceDAO extends DAO<Balance> {

    private SQLiteDatabase database;

    public BalanceDAO(Context context) {
        super(context);
        campos = new String[]{"id","salario","conta"};
        tableName = "balance";
        database = getWritableDatabase();

    }

    public Balance getByNome(String nome) {
        Balance balance = null;

        Cursor cursor = executeSelect("nome = ?", new String[]{nome}, null);

        if(cursor!=null && cursor.moveToFirst())
        {
            balance = serializeByCursor(cursor);
        }
        if(!cursor.isClosed())
        {
            cursor.close();
        }


        return balance;
    }

    public Balance getByID(Integer id) {
        Balance balance = null;

        Cursor cursor = executeSelect("id = ?", new String[]{String.valueOf(id)}, null);

        if( cursor!=null && cursor.moveToFirst() )
        {
            balance = serializeByCursor( cursor );
        }
        if( !cursor.isClosed() )
        {
            cursor.close();
        }

        return balance;
    }

    public List<Balance> listAll() {
        List<Balance> list = new ArrayList<Balance>();
        Cursor cursor = executeSelect(null, null, "1");

        if(cursor!=null && cursor.moveToFirst())
        {
            do{
                list.add(serializeByCursor(cursor));
            }while(cursor.moveToNext());

        }
        if(!cursor.isClosed())
        {
            cursor.close();
        }

        return list;

    }

    public boolean salvar(Balance balance)
    {
        ContentValues values = serializeContentValues(balance);
        if(database.insert(tableName, null, values)>0)
            return true;
        else
            return false;
    }

    public boolean deletar(Integer id)
    {
        if(database.delete(tableName, "id = ?", new String[]{String.valueOf(id)})>0)
            return true;
        else
            return false;
    }

    public boolean atualizar(Balance balance)
    {
        ContentValues values = serializeContentValues(balance);
        if(database.update(tableName, values, "id = ?", new String[]{String.valueOf(balance.getId())})>0)
            return true;
        else
            return false;
    }

    private Balance serializeByCursor(Cursor cursor)
    {
        Balance balance = new Balance();
        balance.setId(cursor.getInt(0));
        balance.setIncome(cursor.getDouble(1));
        balance.setBill(cursor.getDouble(2));

        return balance;
    }

    private ContentValues serializeContentValues(Balance balance)
    {
        ContentValues values = new ContentValues();
        values.put("id", balance.getId());
        values.put("salario", balance.getIncome());
        values.put("conta", balance.getBill());

        return values;
    }

    private Cursor executeSelect(String selection, String[] selectionArgs, String orderBy)
    {
        return database.query(tableName,campos, selection, selectionArgs, null, null, orderBy);
    }
}
