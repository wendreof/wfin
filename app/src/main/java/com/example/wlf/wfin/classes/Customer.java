package com.example.wlf.wfin.classes;

import java.io.Serializable;

public class Customer implements Serializable{

    private String fulName;
    private int age;
    private String email;

    public String getFulName() {
        return fulName;
    }

    public void setFulName(String fulName) {
        this.fulName = fulName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Nome: " + fulName + '\n' +
                "Idade: " + age + '\n' +
                "E-mail: " + email;
    }
}
