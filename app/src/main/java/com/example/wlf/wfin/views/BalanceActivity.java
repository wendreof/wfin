package com.example.wlf.wfin.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wlf.wfin.adapter.BalanceListAdapter;
import com.example.wlf.wfin.R;
import com.example.wlf.wfin.classes.Balance;
import com.example.wlf.wfin.dao.BalanceDAO;
import com.example.wlf.wfin.utils.MaskEditUtil;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class BalanceActivity extends AppCompatActivity implements Serializable, View.OnClickListener {

    private Balance balance;
    private List<Balance> balances;
    private BalanceDAO dao;
    private TextView customerName;
    private EditText bill;
    private EditText income;
    private TextView saldo;
    private Button btnSaveBalance;
    private Spinner spinnerIncomeType;
    private Spinner spinnerBillType;
    private ListView lvBalances;
    private Button btnNewBalance;
    private String[] incomeType = { "Renda principal", "Bônus", "Outras Rendas" };
    private String[] billType = { "Cartão de crédito", "Alimentação", "Compras", "Moradia", "Transporte" };
    private Button btnSearch;
    private EditText editSearch;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_balance );

        findViewByIds();

        setListeners();

        fillFields();

        balance = new Balance();

        balances = new ArrayList<Balance>();

        dao = new BalanceDAO(getApplicationContext());

        clearFields();
        updateList();
        addTextChangedListener();
    }

    public void findViewByIds()
    {
        customerName = findViewById( R.id.customerName );
        bill = findViewById( R.id.bill );
        income = findViewById( R.id.income );
        saldo = findViewById( R.id.saldo );
        spinnerIncomeType = findViewById( R.id.spinnerIncomeType );
        spinnerBillType = findViewById( R.id.spinnerBillType );
        btnSaveBalance = findViewById( R.id.btnSaveBalance );
        btnNewBalance = findViewById(R.id.btnNewBalance);
        lvBalances = findViewById( R.id.lvBalances );
        btnSearch = findViewById( R.id.btnSearch );
        editSearch = findViewById( R.id.editSeach );
    }

    public void setListeners()
    {
        bill.setOnClickListener( this );
        income.setOnClickListener( this );
        btnSearch.setOnClickListener( this );
        editSearch.setOnClickListener( this );
        btnSaveBalance.setOnClickListener( this );
        btnNewBalance.setOnClickListener( this );
        lvBalances.setOnItemClickListener( selecionarBalance );
        lvBalances.setOnItemLongClickListener( deleteBalance );
    }

    @Override
    public void onClick( View view ) {

        int a = view.getId();

        if ( a == R.id.btnSaveBalance )
        {
            newBalance();
            clearFields();
        }
        else if ( a == R.id.btnNewBalance)
        {
            updateBalance();
            clearFields();
        }
        else
        {
            if( ( editSearch.getText() != null ) && ( editSearch.getText().length() > 0 ) )
            {
                btnSearch();
                showMSG("Buscando ..");
            }
            else
            {
                showToastMSG( "Por favor informe todos os valores! ;)");
            }
        }
    }

    public void newBalance()
    {
        if( ( bill.getText() != null ) && ( bill.getText().length() > 0 ) &&
                ( income.getText() != null ) && ( income.getText().length() > 0) )
        {
            balance.setBill( Double.parseDouble(bill.getText().toString()) );
            balance.setIncome( Double.parseDouble(income.getText().toString()) );
            saldo.setText( String.valueOf(balance.getSaldo()) );
            balance.setIncomeType( incomeType[spinnerIncomeType.getSelectedItemPosition()] );
            balance.setBillType( billType[spinnerBillType.getSelectedItemPosition()] );

            dao.salvar( balance );
            updateList();
            showToastMSG("Registro CADASTRADO! :)");
        }
        else
        {
            showToastMSG( "Por favor informe todos os valores! ;)" );
        }
    }

    public void updateBalance()
    {
        if( ( bill.getText() != null ) && ( bill.getText().length() > 0 ) &&
                ( income.getText() != null ) && ( income.getText().length() > 0) )
        {
            balance.setBill(Double.parseDouble( bill.getText().toString()) );
            balance.setIncome(Double.parseDouble( income.getText().toString()) );
            saldo.setText( String.valueOf(balance.getSaldo()) );
            balance.setIncomeType( incomeType[spinnerIncomeType.getSelectedItemPosition()] );
            balance.setBillType( billType[spinnerBillType.getSelectedItemPosition()] );

            dao.atualizar( balance );
            updateList();
            showToastMSG("Registro ATUALIZADO! ;)");
        }
        else
        {
            showToastMSG( "Por favor informe todos os valores! ;)" );
        }
    }

    public void fillFields()
    {
        Bundle e = getIntent().getExtras();

        if ( e != null )
        {
            balance =  ( Balance ) e.getSerializable( "balance" );

            if ( balance != null )
            {
                customerName.setText(String.valueOf( balance.getCustomer()) );
            }
        }
       fillIncomeType();
       fillBillType();
    }

    public void fillIncomeType()
    {
        try
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, incomeType );
            adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
            spinnerIncomeType.setAdapter( adapter );
        }
        catch ( Exception ex )
        {
            showToastMSG( "Erro" + ex.getMessage() );
        }
    }

    public void fillBillType()
    {
        try
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, billType );
            adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
            spinnerBillType.setAdapter( adapter );
        }
        catch ( Exception ex )
        {
            showToastMSG( "Erro" + ex.getMessage() );
        }
    }

    private void showToastMSG( String msg )
    {
        Toast.makeText( this, msg, Toast.LENGTH_LONG ).show();
    }

    private AdapterView.OnItemClickListener selecionarBalance = new AdapterView.OnItemClickListener() {

        public void onItemClick( AdapterView<?> arg0, View arg1, int pos, long id ) {
            balance = balances.get( pos );
            fillData( balance );
        }
    };

    private AdapterView.OnItemLongClickListener deleteBalance = new AdapterView.OnItemLongClickListener() {

        public boolean onItemLongClick( AdapterView<?> arg0, View arg1, int pos, long arg3 ) {
            deleteBalance( balances.get(pos).getId() );
            return true;
        }
    };

    private void updateList() {

        balances = dao.listAll();
        if ( balances != null ) {
            if ( balances.size() > 0 ) {
                BalanceListAdapter pla = new BalanceListAdapter( getApplicationContext(), balances );
                lvBalances.setAdapter( pla );
            }
        }
    }

    private void btnSearch()
    {
            int idBalance = Integer.parseInt(editSearch.getText().toString());

            fillData( dao.getByID(idBalance) );
    }

    private void fillData( Balance balance ) {
        income.setText( String.valueOf(balance.getIncome()) );
        bill.setText(String.valueOf( balance.getBill()) );
        //spinnerIncomeType.setSelection(balance.getIncomeType().equalsIgnoreCase("Renda principal") ? 0 : 1);
        //spinnerBillType.setSelection(balance.getBillType().equalsIgnoreCase("Cartão de crédito") ? 0 : 1);
    }

    private void deleteBalance( final int idBalance ) {


        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( "Excluir registro?" )
                .setIcon (android.R.drawable.ic_dialog_alert )
                .setMessage( "Deseja mesmo excluir esse registro?" )
                .setCancelable( false )
                .setPositiveButton( getString(R.string.sim ),
                        new DialogInterface.OnClickListener() {
                            public void onClick( DialogInterface dialog, int id ) {
                                if ( dao.deletar( idBalance) ) {
                                    updateList();
                                    showMSG(getString( R.string.msgExclusao) );
                                } else {
                                    showMSG(getString( R.string.msgFalhaExclusao) );
                                }
                            }
                        })
                .setNegativeButton(getString( R.string.nao ),
                        new DialogInterface.OnClickListener() {
                            public void onClick( DialogInterface dialog, int id ) {
                                dialog.cancel();
                            }
                        });
        builder.create();
        builder.show();
        updateList();
    }

    private void showMSG( String msg ) {
        Toast.makeText( getApplicationContext(), msg, Toast.LENGTH_LONG ).show();
    }

    private void clearFields() {
        bill.setText( "" );
        income.setText( "" );
      // saldo.setText("");
    }

    public void addTextChangedListener()
    {
        income.addTextChangedListener(MaskEditUtil.mask( income, "####.##") );
        bill.addTextChangedListener(MaskEditUtil.mask( bill, "####.##") );
    }
}
